# coding=utf-8
"""
==================================================
explorer
==================================================
This module provides a backward algorithm that computes the binary and levelled permissiveness. The binary
permissiveness computes whether the permissiveness is greater than a threshold. The levelled computes the closest lower
 threshold of the permissiveness among a list of threholds.

Classes:
------

Methods:
------
"""
from __future__ import annotations
from dataclasses import dataclass
from math import inf
from typing import List

import ppl
from ppl.polyhedron import Polyhedron

from pysymbrobustness.dtype import Location, Valuation, Threshold
from pysymbrobustness.ppl_extension.polyhedra import c_polyhedron_constructor, equal
from pysymbrobustness.dtype import Location, Valuation, Threshold
from pysymbrobustness.ppl_extension.polyhedra import c_polyhedron_constructor, equal
from pysymbrobustness.ta.timedauto import TimedAutomaton, Configuration
from pysymbrobustness.ppl_extension.variable_elimination import one_variable_elimination



BINARY_PERMISSIVENESS_SET = "Binary permissiveness set"


@dataclass(init=True)
class LvlPermValue(object):
    """
    This class represents the numeric value of a levelled permissiveness for an interval [lower_bound, upper_bound[
    The boolen value is True if the value is in [lower_bound, upper_bound[. False otherwise.
    """
    lower_bound: threshold
    upper_bound: threshold
    value: bool

    def __eq__(self, other_lvl_perm: LvlPermValue) -> bool:
        return self.lower_bound == other_lvl_perm.lower_bound and \
               self.upper_bound == other_lvl_perm.upper_bound \
               and self.value == other_lvl_perm.value

    def __repr__(self):
        return f"Interval: [{self.lower_bound}, {self.upper_bound}[, value: {self.value}\n"


@dataclass(init=True)
class BinPermFct(object):
    """
    This class represents a binary permissiveness function, associated with a threshold threshold, for a given location.
    value is the set of polyhedra of the winning valuations such that its permissiveness is greater than threshold
    for the given location.
    """
    threshold: threshold
    value: ppl.C_Polyhedron

    def __eq__(self, other_bin_perm: BinPermFct):
        return self.threshold == other_bin_perm.threshold and self.value == other_bin_perm.value

    def __repr__(self):
        return f"threshold: {self.threshold}, value: {self.value.constraints()}\n"


def evaluation(binary_fct: List[Polyhedron], value: Valuation) -> bool:
    """
    @param binary_fct: a List of polyhedra
    @param value: a valuation value
    @return: True if the valuation is one of the polyhedra of binary_fct, False otherwise.
    """
    # reduce(lambda acc, polyh: acc or value in polyh, fct)
    for polyh in binary_fct:
        if value in polyh:
            return True
    return False


class Explorer(object):

    def __init__(self, ta: TimedAutomaton,
                 to_print: bool = False):
        self.ta = ta
        self.to_print = to_print
        self.executed = False

    def start_value(self, node: Location) -> ppl.C_Polyhedron:
        """
        @param node: a Location
        @return: The value of the binary function, B_0 on location node, as a unique polyhedron.
        NB: we can return a unique polyhedron, as the value of B_0 is always a unique polyhedron
        (no constraints, or empty poly) even in acyclic cases.
        """

        # If we are on goal location, the set of winning valuation is the whole space.
        # Else, we initialize to the empty polyhedron.
        if node != self.ta.goal_location:
            return ppl.C_Polyhedron(self.ta.number_clocks, 'empty')
        else:
            return c_polyhedron_constructor(dimension=self.ta.number_clocks)

    def compute_levelled_permissiveness(self, thresholds: List[Threshold], configuration: Configuration) -> \
            tuple[Threshold, Threshold]:
        """
        this function returns the levelled permissiveness value for a specific configuration (location, valuation)
        Input:
        :thresholds: a list of thresholds to evaluate whether the permissiveness is lower or higher than each threshold.
        :configuration: a configuration on which we evaluate the permissiveness.
        Output: an interval of threshold [th_0, th_1], where th_0, ... ,th_n is the sorted list of thresholds, such that
         the permissiveness of the configuration is between th_0 and th_1

        """
        sorted_thresholds = sorted(thresholds)
        # sorting the thresholds to compute the lowest thresholds first.
        # sorted threshold: th_0...th_n
        levelled_perm: Threshold = - inf  # initializing the permissiveness to - inf.
        inf_perm: Threshold = - inf
        sup_perm: Threshold = + inf
        levelled_perm: Threshold = - inf  # initializing the permissiveness to - inf.
        inf_perm: Threshold = - inf
        sup_perm: Threshold = + inf
        for th in sorted_thresholds:
            perm = self.compute_binary_permissiveness(location=configuration.location, th=th)
            # computing the binary permissiveness of the location for the threshold th
            # perm is then a polyhedron, such that is configuration.valuation belongs to perm, then its permissiveness
            # is higher than th.

            # cond_1: we found a threshold such that the permissiveness is >= th
            if configuration.valuation in perm:
                inf_perm = th

                # cond_2: we found a threshold such that the permissiveness is < th
            if not configuration.valuation in perm:
                sup_perm = th
                return inf_perm, sup_perm
                # as soon as we found a threshold s.t perm < th, we can say that the perm is between the last threshold
                # th_last found s.t perm >= th_last, as the list of threshold is sorted. We do not have to check other
                # thresholds, as they are higher, and we can return the last threshold as the minimal permissiveness
                # and the current as the maximum threshold.
                # NB: if not previous threshold was found, then we just return inf_perm = - inf (as it was initialize)

        return inf_perm, sup_perm
        # this returns happens when no supremum was found, sup_perm then values + inf, as in the initialization.

    def compute_from_successor(self, location: Location, successor: Location, threshold: threshold) -> ppl.C_Polyhedron:
        edge = self.ta[location][successor]
        label = list(edge.values())[0]
        reset = label.resets
        guard = label.guard
        succ_poly = self.ta.nodes[successor][BINARY_PERMISSIVENESS_SET]
        dim = self.ta.number_clocks

        poly = one_variable_elimination(poly_p=succ_poly, resets=reset, guard=guard, dim=dim, threshold=threshold)
        poly.minimized_constraints()

        return poly

    def compute_binary_permissiveness(self, location: Location, th: threshold) -> ppl.C_Polyhedron:
        """
        location: the location on which we compute the permissiveness
        threshold: the threshold to check if perm >= p
        """
        # dim = self.ta.number_clocks

        if self.executed:
            return self.ta.nodes[location][BINARY_PERMISSIVENESS_SET]

        if not self.ta.is_acyclic() or not self.ta.is_branch_free():
            raise NotImplemented

        # Initialization of the permissiveness sequence # OK
        for node in self.ta.nodes:
            self.ta.nodes[node][BINARY_PERMISSIVENESS_SET] = self.start_value(node)
            self.ta.nodes[node][BINARY_PERMISSIVENESS_SET] = self.start_value(node)

        # Computation of fixed point of the permissiveness sequence for each nodes
        successor_node = self.ta.goal_location
        while successor_node != location:  # While we have still nodes to explore, continue
            # current_node = nodes.pop()

            current_node = self.ta.predecessors(successor_node).__next__()

            current_node_poly = self.compute_from_successor(location=current_node, successor=successor_node,
                                                            threshold=th)
            self.ta.nodes[current_node][BINARY_PERMISSIVENESS_SET] = current_node_poly
            successor_node = current_node

        self.executed = True
        return self.ta.nodes[location][BINARY_PERMISSIVENESS_SET]

        # one_variable_elimination(poly_p: ppl.C_Polyhedron, resets: List[int], guard: ppl.C_Polyhedron, dim: int,
        # threshold: Threshold) -> ppl.C_Polyhedron:

    def explorer(self, threshold: Threshold):
        init_location = self.ta.init_location
        self.compute_binary_permissiveness(location=init_location, th=threshold)

    def compute_final_binary_permissiveness(self, location: Location, threshold: Threshold) -> ppl.C_Polyhedron:
        self.explorer(threshold=threshold)
        return self.ta.nodes[location][BINARY_PERMISSIVENESS_SET]

    def bin_perm_configuration(self, configuration: Configuration, threshold: Threshold) -> bool:
        return configuration.valuation in self.compute_binary_permissiveness(location=configuration.location,
                                                                             th=threshold)

    def compute_global_levelled_permissiveness(self, location: Location, thresholds: List[Threshold]) -> \
            List[BinPermFct]:
        return [BinPermFct(threshold=th, value=self.compute_binary_permissiveness(location=location, th=th))
                for th in thresholds]
