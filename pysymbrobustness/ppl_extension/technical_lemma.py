# coding=utf-8
from __future__ import annotations  # For forward reference typing

from copy import deepcopy
from types import FunctionType
from typing import Tuple, List, NamedTuple

import ppl
import pysymbrobustness.ppl_extension.linear_algebra as lin_alg
import pysymbrobustness.ppl_extension.rational_linear_algebra as rla
import pysymbrobustness.ppl_extension.piecewise_linear_function as plf
from fractions import Fraction

Point2D = Tuple[plf.LinearFunction, plf.LinearFunction]


class CaseData(NamedTuple):
    name: str
    c_list: List[ppl.Constraint]
    alpha_opt: plf.LinearFunction
    beta_opt: plf.LinearFunction


def get_coefficient(fct: rla.RationalLinearExpression) -> Tuple[
    Fraction, rla.RationalLinearExpression]:
    """
    From a function fct = b, returns the tuple a, b where a is the sum of the coefficients of b.
    @param fct:
    @type fct:
    @return:
    @rtype:
    """
    if type(fct) != rla.RationalLinearExpression:
        raise TypeError

    b = fct
    a = sum(fct.coefficients())

    return a, b


def compute_permissiveness(poly: ppl.C_Polyhedron, f: plf.LinearFunction,
                           g: plf.LinearFunction,
                           alpha_opt: plf.LinearFunction,
                           beta_opt: plf.LinearFunction):
    if (type(f) != rla.InfiniteExpression and type(
            f) != rla.RationalLinearExpression) or \
            (type(g) != rla.InfiniteExpression and type(
                g) != rla.RationalLinearExpression):
        raise TypeError

    f_linear = type(f) == rla.RationalLinearExpression
    g_linear = type(g) == rla.RationalLinearExpression

    h_opt = plf.Spline(plf.SubSpline(polyhedron=poly,
                                     function=beta_opt - alpha_opt))

    if f_linear and g_linear:
        a, b = get_coefficient(f)
        c, d = get_coefficient(g)
        f_opt = plf.Spline(
            plf.SubSpline(polyhedron=poly, function=a * alpha_opt + b))
        g_opt = plf.Spline(
            plf.SubSpline(polyhedron=poly, function=c * beta_opt + d))
        return f_opt.minimum_list([g_opt, h_opt])

    elif not f_linear and not g_linear:
        return h_opt
    else:
        a, b = get_coefficient(
            f if f_linear else g)
        # alpha if f_linear else beta)
        opt = alpha_opt if f_linear else beta_opt
        spline = plf.Spline(
            plf.SubSpline(polyhedron=poly, function=a * opt + b))
        return h_opt.minimum(spline)


def opti_f_inf_g_inf(f: plf.LinearFunction, g: plf.LinearFunction,
                     entry_poly: ppl.C_Polyhedron,
                     entry_interval: Tuple[
                         plf.LinearFunction, ...]) -> plf.Spline:
    min_alpha, max_alpha, min_beta, max_beta = entry_interval

    return compute_permissiveness(poly=entry_poly, f=f, g=g,
                                  alpha_opt=min_alpha, beta_opt=max_beta)


def name_to_find(cs_list: List[ppl.Constraint], poly: ppl.C_Polyhedron,
                 f: plf.LinearFunction, g: plf.LinearFunction,
                 alpha_opt: plf.LinearFunction,
                 beta_opt: plf.LinearFunction) -> plf.Spline:
    cs = lin_alg.cs_from_list(cs_list)
    p_0 = deepcopy(poly)
    p_0.add_constraints(cs)
    return compute_permissiveness(poly=p_0, f=f, g=g, alpha_opt=alpha_opt,
                                  beta_opt=beta_opt)


def a_c_same_sign(f: plf.LinearFunction, g: plf.LinearFunction,
                  entry_poly: ppl.C_Polyhedron,
                  entry_interval: Tuple[plf.LinearFunction, ...]) -> plf.Spline:
    min_alpha, max_alpha, min_beta, max_beta = entry_interval
    a, b = get_coefficient(f)
    c, d = get_coefficient(g)

    permissiveness = plf.Spline()

    if a >= 0 and c >= 0:
        # Positive case
        middle_rle = (max_beta - b) * Fraction(1, a+1)

        constraint_list = [
            ([middle_rle <= min_alpha], min_alpha),
            ([min_alpha <= middle_rle, middle_rle <= max_alpha], middle_rle),
            # NB: Generates the 1/2 function for the (0,0), (1,1), (1,0) triangle
            ([max_alpha <= middle_rle], max_alpha)
        ]

        for constraints, opt in constraint_list:
            alpha_opt = opt
            beta_opt = max_beta

            spline = name_to_find(constraints, poly=entry_poly, f=f, g=g,
                                  alpha_opt=alpha_opt, beta_opt=beta_opt)
            permissiveness.fusion(other=spline)

    else:
        # Negative case
        middle_rle = (min_alpha + d) * Fraction(1, 1 - c)

        constraint_list = [
            ([max_beta <= middle_rle], max_beta),
            ([min_beta <= middle_rle, middle_rle <= max_beta], middle_rle),
            # NB: Generates the 1/2 function for the (0,0), (1,1), (1,0) triangle
            ([middle_rle <= min_beta], min_beta)
        ]

        for constraints, opt in constraint_list:
            alpha_opt = min_alpha
            beta_opt = opt

            spline = name_to_find(constraints, poly=entry_poly, f=f, g=g,
                                  alpha_opt=alpha_opt, beta_opt=beta_opt)
            permissiveness.fusion(other=spline)

    return permissiveness


def opti_f_inf_g_rle(f: plf.LinearFunction, g: plf.LinearFunction,
                     entry_poly: ppl.C_Polyhedron,
                     entry_interval: Tuple[
                         plf.LinearFunction, ...]) -> plf.Spline:
    min_alpha, max_alpha, min_beta, max_beta = entry_interval
    c, d = get_coefficient(g)
    if c >= 0:
        return compute_permissiveness(poly=entry_poly, f=f, g=g,
                                      alpha_opt=min_alpha, beta_opt=max_beta)
    elif c <= 0:
        # Same case a f and g rational linear expression, and c <= 0 and a <= 0
        return a_c_same_sign(f, g, entry_poly, entry_interval)


def opti_f_rle_g_inf(f: plf.LinearFunction, g: plf.LinearFunction,
                     entry_poly: ppl.C_Polyhedron,
                     # alpha: ppl.Variable, beta: ppl.Variable,
                     entry_interval: Tuple[
                         plf.LinearFunction, ...]) -> plf.Spline:
    a, b = get_coefficient(f)
    min_alpha, max_alpha, min_beta, max_beta = entry_interval
    if a <= 0:
        return compute_permissiveness(poly=entry_poly, f=f, g=g,
                                      alpha_opt=min_alpha, beta_opt=max_beta)
    elif a >= 0:
        # Same case a f and g rational linear expression, and c >= 0 and a >= 0
        return a_c_same_sign(f, g, entry_poly, entry_interval)


def a_pos_c_neg(f: plf.LinearFunction, g: plf.LinearFunction,
                entry_poly: ppl.C_Polyhedron,
                entry_interval: Tuple[plf.LinearFunction, ...]):
    a, b = get_coefficient(f)
    c, d = get_coefficient(g)
    min_alpha, max_alpha, min_beta, max_beta = entry_interval

    def f_ev(P: Point2D) -> rla.RationalLinearExpression:
        return a * P[0] + b

    def g_ev(P: Point2D) -> rla.RationalLinearExpression:
        return c * P[1] + d

    def h_ev(P: Point2D) -> rla.RationalLinearExpression:
        return P[1] - P[0]

    def min_ev(min_fct: FunctionType[Point2D, rla.RationalLinearExpression],
               other_fct: List[FunctionType[Point2D, rla.RationalLinearExpression]], P: Point2D) -> List[
        ppl.Constraint]:
        return list(map(lambda fct: min_fct(P) <= fct(P), other_fct))

    def min_f(P: Point2D) -> List[ppl.Constraint]:
        return min_ev(f_ev, [g_ev, h_ev], P)

    def min_g(P: Point2D) -> List[ppl.Constraint]:
        return min_ev(g_ev, [f_ev, h_ev], P)

    def min_h(P: Point2D) -> List[ppl.Constraint]:
        return min_ev(h_ev, [g_ev, f_ev], P)

    # Representation of the entry set trapeze A-B-E-C-D
    A = (min_alpha, max_beta)
    B = (max_alpha, max_beta)
    C = (min_alpha, min_beta)

    def D_pt(alpha_sup_beta: bool):
        return (min_beta, min_beta) if alpha_sup_beta else (max_alpha,
                                                            min_beta)

    def E_pt(alpha_sup_beta: bool):
        return (max_alpha, max_alpha) if alpha_sup_beta else (max_alpha,
                                                              min_beta)

    T_den: Fraction = Fraction(1, a * c + c - a)
    T_alpha, T_beta = (b * (1 - c) - d) * T_den, (b - d * (a + 1)) * T_den

    def alpha_beta_comp(alpha_sup_beta: bool):
        return max_alpha >= min_beta if alpha_sup_beta else min_beta >= max_alpha

    case_1 = CaseData(
        name="case_1",
        c_list=[
            *min_h(A)  # h minimal on A
        ],
        alpha_opt=min_alpha,
        beta_opt=max_beta
    )

    case_2 = CaseData(
        name="case_2",
        c_list=[
            *min_f(B)  # f minimal on B
        ],
        alpha_opt=max_alpha,
        beta_opt=max_beta
    )

    case_3 = CaseData(
        name="case_3",
        c_list=[
            *min_g(C)  # g minimal on C
        ],
        alpha_opt=min_alpha,
        beta_opt=min_beta
    )

    case_4 = CaseData(
        name="case_4",
        c_list=[
            T_beta >= max_beta,  # no g zone
            *min_f(A),  # f minimal on A
            *min_h(B)  # g minimal en B
        ],
        alpha_opt=(max_beta - b) * Fraction(1, a + 1),
        beta_opt=max_beta
    )

    case_5 = CaseData(
        name="case_5",
        c_list=[
            T_alpha <= min_alpha,  # no f zone
            *min_g(A),  # g minimal on A
            *min_h(C)  # h minimal on C
        ],
        alpha_opt=min_alpha,
        beta_opt=(min_alpha + d) * Fraction(1, 1 - c)
    )

    cases_6 = [CaseData(
        name=f"case_6_{alp_sup_beta}",
        c_list=[
            *min_f(C),  # f minimal on C
            alpha_beta_comp(alp_sup_beta),  # alpha_sup_beta = False
            *min_g(D_pt(alp_sup_beta))  # g minimal on D_0
        ],
        alpha_opt=(c * min_beta + d - b) * Fraction(1, a),
        beta_opt=min_beta
    ) for alp_sup_beta in [True, False]]

    cases_7 = [CaseData(
        name=f"case_7_{alp_sup_beta}",
        c_list=[
            *min_g(B),  # g minimal on B
            alpha_beta_comp(alp_sup_beta),  # alpha_sup_beta = False
            *min_f(E_pt(alp_sup_beta))  # f minimal on E_0
        ],
        alpha_opt=max_alpha,
        beta_opt=(a * max_alpha + b - d) * Fraction(1, c)
    ) for alp_sup_beta in [True, False]]

    cases_8 = [CaseData(
        name=f"case_8_{alp_sup_beta}",
        c_list=[
            *min_f(D_pt(alp_sup_beta)),  # f minimal on D_0
            alpha_beta_comp(alp_sup_beta),  # alpha_sup_beta = False
            *min_g(E_pt(alp_sup_beta))  # g minimal on E_0
        ],
        alpha_opt=(b - d) * Fraction(1, c - a),
        beta_opt=(b - d) * Fraction(1, c - a)
    ) for alp_sup_beta in [True, False]]

    cases_9 = [CaseData(
        name=f"case_9_{alp_sup_beta}",
        c_list=[
            T_alpha >= max_alpha,
            *min_g(B),  # g minimal on B
            alpha_beta_comp(alp_sup_beta),  # alpha_sup_beta = False
            *min_h(E_pt(alp_sup_beta))  # h minimal on E_0
        ],
        alpha_opt=max_alpha,
        beta_opt=(a + 1) * max_alpha + b
    ) for alp_sup_beta in [True, False]]

    cases_10 = [CaseData(
        name=f"case_10_{alp_sup_beta}",
        c_list=[
            T_beta <= min_beta,
            *min_f(C),  # f minimal on C
            alpha_beta_comp(alp_sup_beta),  # alpha_sup_beta = False
            *min_h(D_pt(alp_sup_beta))  # h minimal on D_0
        ],
        alpha_opt=c * min_beta * Fraction(1, a) + (d - b) * Fraction(1, a),
        beta_opt=min_beta
    ) for alp_sup_beta in [True, False]]

    case_11 = CaseData(
        name=f"case_11",
        c_list=[
            max_beta >= T_beta, T_beta >= min_beta,
            max_alpha >= T_alpha, T_alpha >= min_alpha,
            T_beta >= T_alpha
        ],
        alpha_opt=T_alpha,
        beta_opt=T_beta
    )

    cases = [case_1, case_2, case_3, case_4, case_5, *cases_6, *cases_7, *cases_8, *cases_9, *cases_10, case_11]

    permissiveness = plf.Spline()
    for case in cases:
        name, constraints, alpha_opt, beta_opt = case
        spline = name_to_find(constraints, poly=entry_poly, f=f, g=g,
                              alpha_opt=alpha_opt, beta_opt=beta_opt)
        permissiveness.fusion(other=spline)

    return permissiveness


def opti_f_rle_g_rle(f: plf.LinearFunction, g: plf.LinearFunction,
                     entry_poly: ppl.C_Polyhedron,
                     entry_interval: Tuple[
                         plf.LinearFunction, ...]) -> plf.Spline:
    a, b = get_coefficient(f)
    c, d = get_coefficient(g)
    min_alpha, max_alpha, min_beta, max_beta = entry_interval

    if a <= 0 and c >= 0:
        return compute_permissiveness(poly=entry_poly,
                                      f=f, g=g,
                                      alpha_opt=min_alpha, beta_opt=max_beta)

    elif (a >= 0 and c >= 0) or (a <= 0 and c <= 0):
        return a_c_same_sign(f, g, entry_poly, entry_interval)
    elif a > 0 and c < 0:
        return a_pos_c_neg(f, g, entry_poly, entry_interval)


def plf_optimization(
        f: plf.LinearFunction, g: plf.LinearFunction,
        entry_poly: ppl.C_Polyhedron,
        entry_set: Tuple[
            plf.SubSpline, plf.SubSpline, plf.SubSpline, plf.SubSpline]) -> plf.Spline:
    """
    @param f:
    @type f:
    @param g:
    @type g:
    @param entry_poly:
    @type entry_poly:
    @param entry_set:
    @type entry_set:
    @return:
    @rtype:
    """
    entry_interval: Tuple[plf.LinearFunction, ...] = tuple(
        map(lambda x: x.function, entry_set))

    copy_entry_poly = deepcopy(entry_poly)
    for sub in entry_set:
        copy_entry_poly.intersection_assign(sub.polyhedron)

    min_alpha, max_alpha, min_beta, max_beta = entry_interval

    copy_entry_poly.add_constraints(lin_alg.cs_from_list([
        min_alpha <= min_beta, max_alpha <= max_beta,
        min_alpha <= max_alpha, min_beta <= max_beta
    ]))
    # entry_poly = entry_poly.intersection_assign()

    if type(f) == rla.InfiniteExpression and type(g) == rla.InfiniteExpression:
        return opti_f_inf_g_inf(f, g, copy_entry_poly, entry_interval)
    elif type(f) == rla.InfiniteExpression and type(
            g) == rla.RationalLinearExpression:
        return opti_f_inf_g_rle(f, g, copy_entry_poly, entry_interval)
    elif type(f) == rla.RationalLinearExpression and type(
            g) == rla.InfiniteExpression:
        return opti_f_rle_g_inf(f, g, copy_entry_poly, entry_interval)
    elif type(f) == rla.RationalLinearExpression and type(
            g) == rla.RationalLinearExpression:
        return opti_f_rle_g_rle(f, g, copy_entry_poly, entry_interval)
    else:
        raise TypeError
