import math
from functools import reduce
from pathlib import Path
from typing import List, Tuple, Dict
from math import atan2, gcd
from dataclasses import dataclass
import re

import ppl

from pysymbrobustness.ppl_extension.piecewise_linear_function import Spline, \
    SubSpline, LinearFunction
from pysymbrobustness.ppl_extension.rational_linear_algebra import \
    RationalLinearExpression


@dataclass
class Point(object):
    x: int
    y: int
    lcm: int

    @property
    def rx(self):
        return self.x / self.lcm

    @property
    def ry(self):
        return self.y / self.lcm

    def __add__(self, other):
        new_gcd = gcd(self.lcm, other.lcm)
        new_lcm = self.lcm * other.lcm // new_gcd

        return Point(
            self.x * (other.lcm // new_gcd) + other.x * (self.lcm // new_gcd),
            self.y * (other.lcm // new_gcd) + other.y * (self.lcm // new_gcd),
            new_lcm)

    def __neg__(self):
        return Point(-self.x, -self.y, self.lcm)

    def __sub__(self, other):
        return self + (-other)

    def __floordiv__(self, other):
        return Point(self.x, self.y, self.lcm * other)

    def __str__(self):
        return f"({2 * self.x} / {self.lcm}, {2 * self.y} / {self.lcm})"

    def __repr__(self):
        return f"Point({self.x}, {self.y}, {self.lcm})"


def visual(perm: Spline) -> List[
    Tuple[RationalLinearExpression, ppl.constraint.Constraint_System]]:
    return list(map(lambda p: (p.function, p.polyhedron.minimized_generators()),
                    perm.sub_splines))


COLORS = ['blue!50', 'green!50',
          'yellow!50!black', 'violet',
          'blue!75', 'green!75',
          'teal', 'lime!50!black', 'olive',
          'cyan', 'white', 'gray']
COLORS += [f'red!{j}' for j in range(1, 101, 1)]
COLORS += [f'blue!{j}' for j in range(1, 101, 1) if j != 50]
COLORS += [f'green!{j}' for j in range(1, 101, 1) if j != 50 and j != 75]


def transform_points_order(coords: List[Point]) -> List[Point]:
    p1 = coords[0]
    p2 = coords[1]
    middle_coord = (p1 + p2) // 2

    new_coords = list(map(lambda c: c - middle_coord, coords))
    angles = list(enumerate(map(lambda c: atan2(c.rx, c.ry), new_coords)))
    sorted_angles = sorted(angles, key=lambda t: t[1])

    return [coords[i] for i, _ in sorted_angles]


def generator_to_points(sub: SubSpline) -> List[Point]:
    point_list = []

    sub.polyhedron.minimized_constraints()

    for point in sub.polyhedron.minimized_generators():
        lcm = int(point.divisor().digits())
        x = int(point.coefficients()[0].digits())
        y = int(point.coefficients()[1].digits())

        point_list.append(Point(x, y, lcm))

    return point_list


@dataclass
class SubSplineTikzData(object):
    n: int
    color: str
    sub_spline: SubSpline

    @staticmethod
    def func_repr(sub_spline: SubSpline) -> Tuple[int, int, int, int]:
        f = sub_spline.function
        return (f.integer_coefficient(ppl.Variable(0)),
                f.integer_coefficient(ppl.Variable(1)),
                f.integer_inhomogeneous_term(),
                f.lcm)


class LatexSplineVisualisation(object):
    def __init__(self, output_file: Path):
        self.output_file: Path = output_file.absolute()
        self.latex: str = ""
        self.colors: Dict[Tuple[int, int, int, int], str] = {}
        self.index_color: int = 0

        self.latex_intro()

    def latex_intro(self):
        self.latex += "\\documentclass{article}\n" \
                      "\\usepackage{amsmath}\n" \
                      "\\usepackage{tikz}\n" \
                      "\\usepackage{array}\n" \
                      "\\usepackage{caption}\n" \
                      "\\usepackage{subfigure}\n" \
                      "\n" \
                      "\\begin{document}\n"

    def latex_new_tikz(self, xsize: int = 2, ysize: int = 2):
        xbar = "0/0"
        for x in range(1, xsize + 1):
            xbar += f",{2 * x}/{x}"

        ybar = "0/0"
        for y in range(1, ysize + 1):
            ybar += f",{2 * y}/{y}"
        self.latex += "\\begin{figure}\n" \
                      "\\begin{subfigure}{12cm}\n" \
                      "\\begin{tikzpicture}\n" \
                      "\\tikzstyle{reg}=[minimum width=2cm, minimum height=2cm, rectangle]\n" \
                      "\n" \
                      "% Draw the scale\n" \
                      f"\\draw[black,->] (-0.2,0) -- ({2 * xsize + 0.25},0) node[right] {{$ x $}};\n" \
                      f"\\draw[black,->] (0,-0.25) -- (0,{2 * ysize + 0.25}) node[above] {{$ y $}};\n" \
                      f"\\draw[loosely dotted] (0,0) grid ({2 * xsize},{2 * ysize});\n" \
                      f"\\foreach \\x/\\xtext in {{ {xbar} }}\n" \
                      f"\\draw[shift={{(\\x,0)}}] (0pt,4pt) -- (0pt,-4pt) " \
                      f"node[below] {{$\\xtext$}};\n" \
                      f"\\foreach \\y/\\ytext in {{ {ybar} }}\n" \
                      f"\\draw[shift={{(0,\\y)}}] (4pt,0pt) -- (-4pt," \
                      f"0pt) node[left] {{$\\ytext$}};\n" \
                      f"% End draw scale\n"

    def latex_tikz_end(self):
        self.latex += "\\end{tikzpicture}\n" \
                      "\\end{subfigure}\n"

    def latex_outro(self):
        self.latex += "\\end{document}\n"

    def legend_intro(self):
        self.latex += "\\begin{subfigure}{12cm}\n" \
                      "$$\n" \
                      "\\begin{array}{|l||l|}\n" \
                      "\\hline\n" \
                      "\\text{Permissiveness function} & \\text{" \
                      "Associated cells} \\\\\n" \
                      "\\hline\n"

    def legend_add_element(self, data: List[SubSplineTikzData]):
        f_repr = data[0].sub_spline.function
        # f_repr = SubSplineTikzData.func_repr(data[0].sub_spline)
        le_repr = str(f_repr)
        le_repr = le_repr.replace('linear expression:', ' ')
        le_repr = le_repr.replace('x0', 'x')
        le_repr = le_repr.replace('x1', 'y')
        le_repr = re.sub(' lcm = [0-9]*', '', le_repr)

        function_repr = f"\\left( {le_repr} \\right) / {f_repr.lcm}"
        list_num = str(data[0].n)
        for d in data[1:]:
            list_num += f", {d.n}"

        self.latex += f"{function_repr} & {list_num} \\\\\n" \
                      "\\hline\n"

    def legend_outro(self, caption: str = ""):
        self.latex += "\\end{array}\n" \
                      "$$\n" \
                      "\\end{subfigure}\n" + \
                      (
                          f"\\caption{{ {caption} }}\n" if caption != "" else "\n") + \
                      "\\end{figure}\n"

    def add_spline(self, spline: Spline, caption: str = ""):
        max_x = 1  # The maximum x coordonate
        max_y = 1  # The maximum y coordonate
        function_map: Dict[
            Tuple[int, int, int, int], List[SubSplineTikzData]] = {}
        tikz_data: List[SubSplineTikzData] = []

        # First extract useful information (colors, existing functions, max_x and max_y)

        for sub in spline.sub_splines:
            f_repr = SubSplineTikzData.func_repr(sub)
            color = None

            if f_repr in self.colors:
                color = self.colors[f_repr]
            else:
                color = COLORS[self.index_color]
                self.index_color += 1
                self.colors[f_repr] = color

            data = SubSplineTikzData(
                sub_spline=sub,
                color=color,
                n=0
            )

            tikz_data.append(data)

            if f_repr not in function_map:
                function_map[f_repr] = []

            function_map[f_repr].append(data)

            for point in generator_to_points(sub):
                max_x = max(max_x, point.rx)
                max_y = max(max_y, point.ry)

        max_x = int(math.ceil(max_x))
        max_y = int(math.ceil(max_y))

        tikz_data.sort(key=lambda d: SubSplineTikzData.func_repr(d.sub_spline))

        self.latex_new_tikz(max_x, max_y)

        tikz = ""
        # Generate the tikz polygone and the index for each subspline
        for n, sub_data in enumerate(tikz_data):
            sub_data.n = n
            color = sub_data.color

            tikz += f"\\draw[fill={color}, opacity=0.5] "
            points = transform_points_order(
                generator_to_points(sub_data.sub_spline))
            for p in points:
                tikz += f"{p} -- "

            tikz += "cycle;\n"

            # Isobarycenter computation
            isobar = sum(points, start=Point(0, 0, 1)) // len(points)
            num_isobar = format(2*isobar.rx, '.2f')
            den_isobar = format(2*isobar.ry, '.2f')
            tikz += f"\\node at ({num_isobar}, {den_isobar}) {{  {n}  }};\n"

        self.latex += tikz
        self.latex_tikz_end()

        # Add the legend
        self.legend_intro()

        for datas in function_map.values():
            self.legend_add_element(datas)

        self.legend_outro(caption)

    def output(self):
        self.latex_outro()
        with open(self.output_file, 'w') as f:
            f.write(self.latex)


class LatexTableSpline(object):
    def __init__(self, output_file: Path):
        self.output_file: Path = output_file.absolute()
        self.latex: str = ""

        self.latex_intro()

    def output(self):
        self.latex_outro()
        with open(self.output_file, 'w') as f:
            f.write(self.latex)

    def latex_intro(self):
        self.latex += """
\\documentclass{article}
\\usepackage{amsmath}
\\usepackage{array}
\\usepackage{caption}
\\usepackage{graphicx}
\\usepackage{multirow}
\\usepackage{longtable}

\\begin{document}\n"""

    def latex_outro(self):
        self.latex += "\\end{document}\n"

    def add_spline(self, spline: Spline, caption: str):
        self.start_spline()

        for subspline in spline.sub_splines:
            self.add_subspline(subspline)

        self.end_spline(caption)

    def add_subspline(self, subspline: SubSpline):
        fct = subspline.function
        poly = subspline.polyhedron

        cs = list(poly.constraints())
        generators = list(poly.minimized_generators())

        n_row = max(len(cs), len(generators))

        for i in range(n_row):
            if i == 0:
                self.latex += f"    \\multirow{{{n_row}}}{{*}}{{ ${self.fct_repr(fct)}$ }}"
            else:
                self.latex += f"\t\t"

            cs_str = self.cs_repr(cs[i]) if i < len(cs) else ""
            generator_str = self.generator_repr(generators[i]) if i < len(generators) else ""
            delim_line = "\\\\" if i == n_row - 1 else "\\\\*"

            self.latex += f"& ${ cs_str }$ & { generator_str } { delim_line } \n"

        self.latex += "    \\hline\n"

    def generator_repr(self, generator) -> str:
        gen_str = str( generator )
        gen_str = gen_str.replace('point(','')
        gen_str = gen_str.replace(')', '')

        return gen_str

    def replace_var(self, s: str) -> str:
        s = re.sub("x0(?=\D|$)", "x", s)
        s = re.sub("x1(?=\D|$)", "y", s)
        s = re.sub("x2(?=\D|$)", "z", s)

        return s

    def cs_repr(self, cs):
        cs_str = str(cs)
        cs_str = self.replace_var( cs_str )

        return cs_str

    def fct_repr(self, fct: LinearFunction) -> str:
        le_str = str(fct.linear_expression)
        le_str = self.replace_var(le_str)
        le_str = le_str.strip()

        if le_str == '0':
            return "0"
        elif fct.lcm == 1:
            return le_str

        return f"\\frac{{ {le_str} }}{{ {fct.lcm} }}"

    def start_spline(self):
        self.latex += """
\\begin{longtable}[h]{|c|c|c|}
    \\hline
    \\endfirsthead
    %
    \\endhead
    %
"""

    def end_spline(self, caption: str):
        self.latex += f"""
    \\caption{{{caption}}}
\\end{{longtable}}
"""


def latex_output(spline, file, tikz_visual: bool = True):
    visualisation = LatexSplineVisualisation(file) if tikz_visual else LatexTableSpline(file)
    visualisation.add_spline(spline, "test")
    visualisation.output()


if __name__ == "__main__":
    pass
